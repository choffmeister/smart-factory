/*
Charles Hoffmeister / Jorge Hernandez / Sydney Smith
Base Smart Factory Control Final
Written in PlatformIO for VSCode

If you are using this code in the Arduino IDE you will need to make sure you delete the "Arduino.h" include file line
and make sure you have the other includes available for this to work properly. 

*/
#include <Arduino.h>
#include <TimerOne.h>
#include <SoftwareSerial.h>
SoftwareSerial mySerial(3,6); //Pin 6 for LCD

const int WATER_SENSOR_PIN = 5; //Analog pin 5
const int WATER_PUMP_PIN = 5;
const int HEATER_PIN = 1;
const int FAN_PIN = 3;
const int THERMISTOR_PIN = 2;
const int A_BUTTON = 11;
const int B_BUTTON = 13;
const int C_BUTTON = 10;
const int D_BUTTON = 12;
const int VT_BUTTON = 9;
const int PIEZO_PIN = 7;
const int PASSCODE = 323;
float currentTemp;
float highTemp,baselineTemp;
bool isFanOn,isHeaterOn,isPumpOn;
bool isPumpDisabled;
bool isPASSCODEEntered = false;

void resetLCD(){
  mySerial.write(254); mySerial.write(1);
}
void LCDControl(float baseLineTemp, float highTemp, float currentTemp, bool isFanOn, bool isHeaterOn, bool isPumpOn){
  mySerial.write(254); mySerial.write(128);
  mySerial.write("Team #6 HBU Engineer");
  mySerial.write(254); mySerial.write(192);
  mySerial.write("LCL=");
  mySerial.write(254); mySerial.write(196);
  mySerial.print(baselineTemp);
  mySerial.write(254); mySerial.write(148);
  mySerial.write("UCL=");
  mySerial.write(254); mySerial.write(152);
  mySerial.print(highTemp);
  mySerial.write(254); mySerial.write(212);
  mySerial.write("Temp=");
  mySerial.write(254); mySerial.write(217);
  mySerial.print(currentTemp);

  mySerial.write(254); mySerial.write(202);
  mySerial.write("*");
  mySerial.write(254); mySerial.write(158);
  mySerial.write("*");
  mySerial.write(254); mySerial.write(222);
  mySerial.write("*");
  mySerial.write(254); mySerial.write(204);
  mySerial.write("Fan: ");
  //Code to check if Fan is on
  if(isFanOn){
    mySerial.write(254); mySerial.write(208);
    mySerial.write("ON ");
  }else{
    mySerial.write(254); mySerial.write(208);
    mySerial.write("OFF");
  }
   
  mySerial.write(254); mySerial.write(160);
  mySerial.write("LED: ");
   //Code to check if the heater is activated 
   if(isHeaterOn){
    mySerial.write(254); mySerial.write(164);
    mySerial.write("ON ");
  }else{
    mySerial.write(254); mySerial.write(164);
    mySerial.write("OFF");
  }
  //Code to check if the pump is ON
  mySerial.write(254); mySerial.write(224);
  mySerial.write("Pump: ");
  if(isPumpOn){
    mySerial.write(254); mySerial.write(229);
    mySerial.write("ON ");
  }else{
    mySerial.write(254); mySerial.write(229);
    mySerial.write("OFF");
  }

}
void intCheckWaterSensor(){
  if(analogRead(WATER_SENSOR_PIN) > 575){
    digitalWrite(WATER_PUMP_PIN,LOW);
    isPumpOn = false;
  }
  if(analogRead(WATER_SENSOR_PIN) < 500){
   if(isPumpDisabled){
     digitalWrite(WATER_PUMP_PIN,LOW);
     isPumpOn = false;
   }else{
     digitalWrite(WATER_PUMP_PIN,HIGH);
     isPumpOn = true;
   }

  }
}
void remoteControl(){
   //Disable LED heater
  if (digitalRead(A_BUTTON) == HIGH)
  {
    delay(100); // delay to debounce switch
    
    //Note is played
    tone(PIEZO_PIN,400,500);
  }
  //Enable LED heater
  else if (digitalRead(B_BUTTON) == HIGH)
  {
    delay(100); // delay to debounce switch
    
    //Note is played
    tone(PIEZO_PIN,600,500);
    highTemp += 0.5;
    mySerial.write(254); mySerial.write(1);
    mySerial.write(254); mySerial.write(198);
    mySerial.write("UCL: +0.5");
    mySerial.write(254); mySerial.write(156);
    mySerial.print(highTemp);
    delay(1500);
    resetLCD();
  }
  //Enable Cooling Fan 
  else if (digitalRead(C_BUTTON) == HIGH)
  {
    delay(100); // delay to debounce switch
    
    //Note is played
    tone(PIEZO_PIN,800,500);
    isPumpDisabled = !isPumpDisabled;
    isPumpOn = false;
    mySerial.write(254); mySerial.write(1);
    
    mySerial.write(254); mySerial.write(195);
    if(isPumpDisabled)
    mySerial.write("PUMP DISABLED");
    if(!isPumpDisabled)
    mySerial.write("PUMP ENABLED");
    delay(1500);
    resetLCD();
  }
  //Disable Cooling Fan
  else if (digitalRead(D_BUTTON) == HIGH)
  {
    delay(100); // delay to debounce switch

    //Note is played
    tone(PIEZO_PIN,1000,500);
    highTemp -= 0.5;
    mySerial.write(254); mySerial.write(1);
    mySerial.write(254); mySerial.write(198);
    mySerial.write("UCL: -0.5");
    mySerial.write(254); mySerial.write(156);
    mySerial.print(highTemp);
    delay(1000);
    resetLCD();
  }
  //No button was pressed
  else 
  {
    //No button was pressed
  }
}
void passCodeCheck(){
 // int PASSCODE = 234; //set value of valid PASSCODE
  int enteredcode = 0;

  //***Enter FIRST digit
  
  //LCD displays the current step
  mySerial.write(254); mySerial.write(1);
  mySerial.write(254); mySerial.write(194);
  mySerial.write("Enter 1ST button");
  //While the button has not been pressed
  while (digitalRead(VT_BUTTON) == LOW)
  {
    delay(250);
    if (digitalRead(A_BUTTON) == HIGH)
    {
      delay(100); // delay to debounce switch
      //LCD displays the A button was pressed
      mySerial.write(254); mySerial.write(150);
      mySerial.write("Button A entered");
      enteredcode = enteredcode + 100;
    }
    if (digitalRead(B_BUTTON) == HIGH)
    {
      delay(100); // delay to debounce switch
      //LCD displays the B button was pressed
      mySerial.write(254); mySerial.write(150);
      mySerial.write("Button B entered");
      enteredcode = enteredcode + 200;
    }
    if (digitalRead(C_BUTTON) == HIGH)
    {
      delay(100); // delay to debounce switch
      //LCD displays the button was pressed
      mySerial.write(254); mySerial.write(150);
      mySerial.write("Button C entered");
      enteredcode = enteredcode + 300;
    }
    if (digitalRead(D_BUTTON) == HIGH)
    {
      delay(100); // delay to debounce switch
      //LCD displays the button was pressed
      mySerial.write(254); mySerial.write(150);
      mySerial.write("Button D entered");
      enteredcode = enteredcode + 400;
    }
  }
  //Note is played
  tone(PIEZO_PIN,500,200);
  delay(2000);

  //***Enter SECOND digit
  //LCD displays the current step
  mySerial.write(254); mySerial.write(1);
  mySerial.write(254); mySerial.write(194);
  mySerial.write("Enter 2ND button");
  //While a button has not been pressed
  while (digitalRead(VT_BUTTON) == LOW)
  {
    delay(250);
    if (digitalRead(A_BUTTON) == HIGH)
    {
      delay(100); // delay to debounce switch
      //LCD displays the A button was pressed
      mySerial.write(254); mySerial.write(150);
      mySerial.write("Button A entered");
      enteredcode = enteredcode + 10;
    }
    if (digitalRead(B_BUTTON) == HIGH)
    {
      delay(100); // delay to debounce switch
      //LCD displays the B button was pressed
      mySerial.write(254); mySerial.write(150);
      mySerial.write("Button B entered");
      enteredcode = enteredcode + 20;
    }
    if (digitalRead(C_BUTTON) == HIGH)
    {
      delay(100); // delay to debounce switch
      //LCD displays the C button was pressed
      mySerial.write(254); mySerial.write(150);
      mySerial.write("Button C entered");
      enteredcode = enteredcode + 30;
    }
    if (digitalRead(D_BUTTON) == HIGH)
    {
      delay(100); // delay to debounce switch
      //LCD displays the D button was pressed
      mySerial.write(254); mySerial.write(150);
      mySerial.write("Button D entered");
      enteredcode = enteredcode + 40;
    }
  }
  delay(2000);
  //Note is played
  tone(PIEZO_PIN,700,200);

  //***Enter THIRD digit
  //LCD displays the current step
  mySerial.write(254); mySerial.write(1);
  mySerial.write(254); mySerial.write(194);
  mySerial.write("Enter 3RD button");
  //While a button has not been pressed
  while (digitalRead(VT_BUTTON) == LOW)
  {
    delay(250);
    if (digitalRead(A_BUTTON) == HIGH)
    {
      delay(100); // delay to debounce switch
      //LCD displays the A button was pressed
      mySerial.write(254); mySerial.write(150);
      mySerial.write("Button A entered");
      enteredcode = enteredcode + 1;
    }
    if (digitalRead(B_BUTTON) == HIGH)
    {
      delay(100); // delay to debounce switch
      //LCD displays the B button was pressed
      mySerial.write(254); mySerial.write(150);
      mySerial.write("Button B entered");
      enteredcode = enteredcode + 2;
    }
    if (digitalRead(C_BUTTON) == HIGH)
    {
      delay(100); // delay to debounce switch
      //LCD displays the C button was pressed
      mySerial.write(254); mySerial.write(150);
      mySerial.write("Button C entered");
      enteredcode = enteredcode + 3;
    }
    if (digitalRead(D_BUTTON) == HIGH)
    {
      delay(100); // delay to debounce switch
      //LCD displays the D button was pressed
      mySerial.write(254); mySerial.write(150);
      mySerial.write("Button D entered");
      enteredcode = enteredcode + 4;
    }
  }
  //Note is played
  tone(PIEZO_PIN,900,200);
  delay(2000);

  //Serial.print("Valid PASSCODE = "); Serial.println(PASSCODE);
  //LCD displays the PASSCODE that was entered
  mySerial.write(254); mySerial.write(1);
  mySerial.write(254); mySerial.write(192);
  mySerial.write("Entered PASSCODE=");
  mySerial.write(254); mySerial.write(209);
  mySerial.print(enteredcode);
  delay(2000);

  if (enteredcode != PASSCODE){
    //LCD diaplys that the PASSCODE was incorrect
    mySerial.write(254); mySerial.write(1);
    mySerial.write(254); mySerial.write(192);
    mySerial.write("INCORRECT PASSCODE!!");
    //Plays an annoying alarm
    for(int x=0;x<10;x++)
    {
      tone(PIEZO_PIN,1000,500);
      delay(750);
    }
    delay(5000);
    passCodeCheck();
}
isPASSCODEEntered = true;
}

void setup() {
  mySerial.begin(9600);
  Serial.begin(9600);//set up to print out text to monitor
  pinMode(WATER_SENSOR_PIN,INPUT);
  pinMode(WATER_PUMP_PIN,OUTPUT);
  pinMode(HEATER_PIN,OUTPUT);
  pinMode(FAN_PIN,OUTPUT);
  pinMode(THERMISTOR_PIN,INPUT);
  baselineTemp = (analogRead(THERMISTOR_PIN) * .145283);
  highTemp = baselineTemp + 2;
  passCodeCheck();
  Timer1.initialize(250000);
  Timer1.attachInterrupt(intCheckWaterSensor);
  mySerial.write(254); mySerial.write(1);
  LCDControl(baselineTemp,highTemp,currentTemp,isFanOn,isHeaterOn,isPumpOn);
}

void loop() {
  
  currentTemp = (analogRead(THERMISTOR_PIN)* .145283);
  LCDControl(baselineTemp,highTemp,currentTemp,isFanOn,isHeaterOn,isPumpOn);
  remoteControl();
  Serial.println((String)"Starting Temp: " + baselineTemp + "Current Temp: " + currentTemp + "Heater Cutoff Temp: " + highTemp);
  //Serial.println("Water Sensor Reading: " + analogRead(WATER_SENSOR_PIN));
  //Serial.println(analogRead(THERMISTOR_PIN));
  if(currentTemp < highTemp){
    //Turn on heater and turn off fan
    digitalWrite(HEATER_PIN,HIGH);
    digitalWrite(FAN_PIN,LOW);
    isHeaterOn = true;
    isFanOn = false;
  }else if(currentTemp > highTemp){
    //Turn off heater and turn on fan 
    digitalWrite(HEATER_PIN,LOW);
    digitalWrite(FAN_PIN,HIGH);
    isHeaterOn = false;
    isFanOn = true;
  }

  delay(500);

  
}
