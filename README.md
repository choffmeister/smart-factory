# Smart Factory

## Introduction
This project is centered around an Arduino and shows how we can use it to control a mini version of a Smart Factory. The Arduino ultimiately controls the following items:


- Heater
- Pump
- Fan 
- Water Sensor
- Speaker
- LCD
- Remote Control 

## Setup
This project was done from within PlatformIO in VSCode. It can be easily opened from within PlatformIO or it can be converted into a standard Arudino IDE project by removing the ```#include <Arduino.h>``` and renaming the ```main.cpp``` to ```main.ino```. You will also need to make sure you have the other include files accessible or added via the Arduino IDE as needed. 

## Usage
Upon bootup the display will prompt for a password. This is hardcoded in the source code file so you can easily find the value that needs to be entered. Upon enter the passcode the display will present current readings from the sensors and will cycle as needed in accordance with the logic in the code. 
